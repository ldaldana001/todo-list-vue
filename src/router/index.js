import Vue from 'vue'
import VueRouter from 'vue-router'
import UndoneTasks from '@/views/UndoneTasks'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'undone',
    component: UndoneTasks
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
